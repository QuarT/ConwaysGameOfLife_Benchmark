libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % "3.0.3" % "test",
        "org.typelevel" %% "spire" % "0.14.1"
    )


organization := "conwaysgameoflife_scala"
name := "default"
version := "0.1-SNAPSHOT"

scalaVersion := "2.12.2"

trapExit := false

mainClass in (Compile, run) := Some("com.github.quar.conwaysgameoflife.ConwaysGameOfLife_For")

//exclude incompleted implementation
excludeFilter in unmanagedSources := "ConwaysGameOfLife_Func.scala"


testFrameworks += new TestFramework(
    "org.scalameter.ScalaMeterFramework")

logBuffered := false


enablePlugins(JmhPlugin)
// enable assembly to uber-jar
mainClass in assembly := Some("org.openjdk.jmh.Main")

