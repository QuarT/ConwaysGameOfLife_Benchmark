Benchmarking ConwaysGameOfLife Scala
====

### Use `sbt-jmh` to benchmarking Different Implementations

```bash
sbt 'jmh:run -i 3 -wi 0 -f1'
```

Snapshot of results
```
[info] # Run complete. Total time: 00:01:12
[info]
[info] Benchmark                         Mode  Cnt   Score   Error  Units
[info] JMHTest.conwaysgameoflife_for     avgt    3  10.303 ± 3.056   s/op
[info] JMHTest.conwaysgameoflife_foropt  avgt    3  10.553 ± 3.275   s/op
[info] JMHTest.conwaysgameoflife_while   avgt    3   2.785 ± 3.940   s/op
```

### Use `sbt-assembly` to export benchmarks into an uber-jar

Require Java 1.8 (because jmh:assembly got some issue with Java 9)

```bash
sbt 'jmh:compile'   # this is required to generate META-INF/BenchmarkList
sbt assembly
```

Then run with:
```
java -jar target/scala-2.12/default-assembly-0.1-SNAPSHOT.jar -wi 0 -i 3 -f 1
```
