package benchmarks

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import com.github.quar.conwaysgameoflife._

@State(Scope.Thread)
@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.SECONDS)
class JMHTest {

    // for loop
    @Benchmark
    def conwaysgameoflife_for: Unit = {
        ConwaysGameOfLife_For.simulate(Array[String]())
    }

    // cfor loop from spire
    @Benchmark
    def conwaysgameoflife_foropt: Unit = {
        ConwaysGameOfLife_ForOpt.simulate(Array[String]())
    }

    // functional way of implementing
    // @Benchmark
    // def conwaysgameoflife_func: Unit = {
    //     ConwaysGameOfLife_Func.simulate(Array[String]())
    // }


    // while loop
    @Benchmark
    def conwaysgameoflife_while: Unit = {
        ConwaysGameOfLife_While.simulate(Array[String]())
    }

}